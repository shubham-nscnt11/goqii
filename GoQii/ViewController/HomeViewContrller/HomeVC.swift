//
//  HomeVC.swift
//  GoQii
//
//  Created by Neosoft on 03/06/24.
//

import UIKit
import CoreData
import UserNotifications

class HomeVC: UIViewController {
    
    // IB Outlets progressview & UILabel
    @IBOutlet weak var lblTotalAmountLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var logInTake: UIButton!
    @IBOutlet var homeBackgroundView: UIView!
    @IBOutlet weak var TableView: UITableView!
    
    
    var totalWaterIntake: Double = 0.0
    var waterDetailsHistory :[WaterIntake] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        scheduleHydrationNotification()
        requestNotificationAuthorization()
        
        // Set the delegate for handling notification-related events
        UNUserNotificationCenter.current().delegate = self
        TableView.delegate = self
        TableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateProgressView()
        
    }
    
    
    // MARK: - UI Setup
    /// Set up the UI elements (e.g., buttons, labels)
    func setUI() {
        homeBackgroundView.backgroundColor = .aquaMist
        logInTake.backgroundColor = .softBlue
        logInTake.applyCornerRadius(8.0)
        logInTake.applyShadow()
        logInTake.applyOpacity(0.8)
        
        TableView.register(UINib(nibName: "WatelogsCell", bundle: nil), forCellReuseIdentifier: "WatelogsCell")    }
    
    // MARK: - Data Fetching and Presentation
    /// Update the label displaying the total water intake for the current day
    func updateDailySummary() {
        
        // Fetch the water intake entries for today
        waterDetailsHistory = WaterIntakeDataManager.shared.FetchWaterIntakeHistory()
        // Calculate the total amount of water intake for today
        let totalAmount = waterDetailsHistory.reduce(0) { $0 + $1.amount }
        // Display the total amount on the label
        lblTotalAmountLabel.text = "\(totalAmount) ml"
        
        waterDetailsHistory.reverse()
        TableView.reloadData()
    }
    
    /// Update the progress view to reflect the daily water intake progress
    func updateProgressView() {
        waterDetailsHistory = WaterIntakeDataManager.shared.FetchWaterIntakeHistory()
        let totalAmount = waterDetailsHistory.reduce(0) { $0 + $1.amount }
        let dailyGoal: Double = 2000 // Assuming a daily goal of 2000 ml
        let progress = Float(min(totalAmount / dailyGoal, 1.0))
        progressView.progress = progress
        updateDailySummary()
    }
    
    // MARK: - User Actions
    /// Action method triggered when the log intake button is tapped
    @IBAction func logIntakeButtonTapped(_ sender: Any) {
        let logWaterIntakeVC = LogWaterIntakeVC()
        logWaterIntakeVC.title = "Log Water Intake"
        navigationController?.pushViewController(logWaterIntakeVC, animated: true)
    }
    
    // MARK: - Notification Handling
    /// Request user authorization for displaying notifications
    func requestNotificationAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { granted, error in
            if granted {
                print("Notification authorization granted")
            } else {
                print("Notification authorization denied: \(error?.localizedDescription ?? "Unknown error")")
            }
        }
    }
    
    /// Schedule hydration notifications to remind the user to drink water
    func scheduleHydrationNotification() {
        let content = UNMutableNotificationContent()
        content.title = NotificationConstants.title
        content.body = NotificationConstants.body
        
        var dateComponents = DateComponents()
        dateComponents.minute = 0
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let request = UNNotificationRequest(identifier: "hydrationNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { error in
            if let error = error {
                print("Error scheduling notification: \(error.localizedDescription)")
            }
        }
        
    }
    // MARK: - Reload list
    func reloadTableView(){
        waterDetailsHistory.reverse()
        TableView.reloadData()
    }
}
extension HomeVC: UNUserNotificationCenterDelegate {
    // MARK: - UNUserNotificationCenterDelegate
    
    /// Handle presentation of notifications when the app is in the foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Display the notification
        completionHandler([.alert, .sound])
    }
    
    // Handle notification response (e.g., user taps on the notification)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Handle the notification response here
        completionHandler()
    }
}



extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return waterDetailsHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WatelogsCell", for: indexPath) as? WatelogsCell
        let amount = String(waterDetailsHistory[indexPath.row].amount)
        cell?.lblTitle.text = "\(amount) ml"
    
        cell?.lblDetails.text = waterDetailsHistory[indexPath.row].date?.toString(dateFormat: "dd-MMM-yyyy")
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        let eventArrayItem = waterDetailsHistory[indexPath.row]
        let context = WaterIntakeDataManager.shared.context

        if editingStyle == .delete {
            context.delete(eventArrayItem)

            do {
                try context.save()
                updateDailySummary()
            } catch let error as NSError {
                print("Error While Deleting Note: \(error.userInfo)")
            }
        }

    }

    
    
}
