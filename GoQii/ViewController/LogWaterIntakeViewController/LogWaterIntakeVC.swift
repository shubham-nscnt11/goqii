//
//  LogWaterIntakeVC.swift
//  GoQii
//
//  Created by Neosoft on 03/06/24.
//

import UIKit
import CoreData

class LogWaterIntakeVC: UIViewController {

    // IB Outlets Text filed & Segment Control
    @IBOutlet weak var tfAmountTextField: UITextField!
    @IBOutlet weak var logInTake: UIButton!
    @IBOutlet weak var unitSegmentedControl: UISegmentedControl!
    @IBOutlet var logInTakeWaterView: UIView!
    
    // Add a property to store the selected unit
    var selectedUnit: String = "ml" // Default unit
    var waterIntakeToEdit: WaterIntake?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        updateUI()

    }
    
    func setupUI() {
        
        if let waterIntake = waterIntakeToEdit {
                    // If editing an existing entry, pre-fill the text field and segmented control
                    tfAmountTextField.text = "\(waterIntake.amount)"
                    if let unit = waterIntake.unit {
                        let segmentIndex = unitSegmentedControl.numberOfSegments - 1
                        for index in 0...segmentIndex {
                            if let title = unitSegmentedControl.titleForSegment(at: index), title == unit {
                                unitSegmentedControl.selectedSegmentIndex = index
                                break
                            }
                        }
                    }
                }
    }
    
    func updateUI () {
        logInTakeWaterView.backgroundColor = .aquaMist
        logInTake.backgroundColor = .softBlue
        logInTake.applyCornerRadius(8.0)
        logInTake.applyShadow()
        logInTake.applyOpacity(0.8)
    }
    
    
    @IBAction func logIntakeButtonTapped(_ sender: Any) {
        
        guard let amountText = tfAmountTextField.text, let amount = Double(amountText) else {
            return
        }
        if amount <= 2000.0{
            WaterIntakeDataManager.shared.saveWaterIntake(_amount: amount)
            
            navigationController?.popViewController(animated: true)
        }else{
            AlertManager.shared.showAlert(on: self, title: "Water Intake", message: "Please select less amount of water")
            
        }
       
    }
    
}
