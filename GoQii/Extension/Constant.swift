//
//  Constant.swift
//  GoQii
//
//  Created by user on 04/06/24.
//

import Foundation

struct NotificationConstants {
    static let title = "Stay Hydrated!"
    static let body = "Reminder: Drink a glass of water!"
}


extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }

}
