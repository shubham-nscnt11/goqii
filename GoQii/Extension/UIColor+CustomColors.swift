//
//  UIColor+CustomColors.swift
//  GoQii
//
//  Created by user on 04/06/24.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hex: String) {
        
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt64 = 0
        
        Scanner(string: hexSanitized).scanHexInt64(&rgb)
        
        let red = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
        let blue = CGFloat(rgb & 0x0000FF) / 255.0
        let alpha: CGFloat = 1.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    // home Dashboard  Colors
    static let softBlue = UIColor(hex: "#1ca3ec")
    static let aquaMist = UIColor(hex: "#DEF4FC")
    static let paleSteel = UIColor(hex: "#bfc8d2")
    static let crimsonBlaze = UIColor(hex: "#F45458")
    static let tranquilGreen = UIColor(hex: "#9DEFAB")
    
    // login take Colors
    static let systemPinkColor = UIColor(hex: "FF2D55")
    static let coralCrush = UIColor(hex: "#FE968A")
    static let tangerineBurst = UIColor(hex: "#FE9654")
}


