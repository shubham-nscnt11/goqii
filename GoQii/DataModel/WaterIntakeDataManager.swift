//
//  WaterIntake.swift
//  GoQii
//
//  Created by Neosoft on 03/06/24.
//

import Foundation
import CoreData

class WaterIntakeDataManager {
  
    
    // Singleton instance of the CoreDataStack.
    static let shared = WaterIntakeDataManager()
    
    
    var waterDetails :[WaterIntake] = []

    init(){}
    
    // Lazily initializes the NSPersistentContainer.
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "GoQii")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            // Handles errors, if any, during persistent store loading.
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    // Saves changes made in the managed object context.
    func saveContext() {
        
        // Checks if the managed object context has any changes.
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func saveWaterIntake(_amount:Double){
        
        let waterIntake = WaterIntake(context: context)
        waterIntake.date = Date()
        waterIntake.amount = _amount
        do {
            try context.save()
        } catch let error as NSError {
            print("Error fetching data: \(error.localizedDescription)")

        }
    }
    
    //take progress
    func FetchWaterIntakeHistory()-> [WaterIntake] {
        
        let fetchRequest: NSFetchRequest<WaterIntake> = WaterIntake.fetchRequest()
        let todayStart = Calendar.current.startOfDay(for: Date())
        let todayEnd = Calendar.current.date(byAdding: .day, value: 1, to: todayStart)!
        fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date < %@", todayStart as NSDate, todayEnd as NSDate)
        
        do {
            let result = try context.fetch(fetchRequest)
           return result
        } catch {
            print("Error fetching data: \(error.localizedDescription)")
        }
        return waterDetails
    }
    
    func deleteWaterIntakeData(waterData:WaterIntake) -> Bool{
            context.delete(waterData)
            do {
                try context.save()
                return true
            } catch let error as NSError {
                return false  
            }
       
        }
}

