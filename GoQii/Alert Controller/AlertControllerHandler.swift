//
//  AlertControllerHandler.swift
//  GoQii
//
//  Created by apple on 04/06/24.
//

import UIKit

class AlertManager {

    // MARK: - Singleton Instance
    static let shared = AlertManager()

    // MARK: - Initializer
    // Private initializer to prevent instantiation
    private init() {}

    // MARK: - Public Methods

    /// Show a simple alert with a title, message, and an OK button
    func showAlert(on viewController: UIViewController, title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }

    /// Show an alert with a title, message, and custom actions
    func showAlert(on viewController: UIViewController, title: String, message: String, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            alertController.addAction(action)
        }
        viewController.present(alertController, animated: true, completion: nil)
    }

    /// Show an action sheet with a title, message, and custom actions
    func showActionSheet(on viewController: UIViewController, title: String?, message: String?, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for action in actions {
            alertController.addAction(action)
        }
        viewController.present(alertController, animated: true, completion: nil)
    }
}
